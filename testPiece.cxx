/**
 * Programme test de Piece
 *
 * @file testPiece.cxx
 */

// Utile pour l'affichage
#include <iostream>
#include "Piece.h"
#include "Joueur.h"
#include "Echiquier.h"

// Pour utiliser les flux de iostream sans mettre "std::" tout le temps.
using namespace std;

bool compare(Piece &a, Piece &b)
{
  return ( (a.x()==b.x()) && (a.y() == b.y()) );
}

/**
 * Programme principal
 */
int main( int argc, char** argv )
{
  // instancie un objet p1 de type Piece
  Piece p1;
  // p1 est une piece blanche de coordonnees (3,3)
  p1.init( 3, 3, true );
  // p2 est une piece noire de coordonnees (4,4)
  Piece p2( 4, 4 , false);

  Piece p3=p1;
  Piece p4;
  p4=p2;

  //Piece tbl[4];

  // On l'affiche
  p1.affiche();
  p2.affiche();

  if (compare(p1,p2))
    cout << "meme position" << endl;
  else
    cout << "differente position" << endl;


  Joueur jb(true);
  Joueur jn(false);
  jb.affiche();
  jn.affiche();

  Echiquier e;
  jb.placerPieces(e);
  jn.placerPieces(e);
  e.affiche();

  // les objets definis dans cette fonction sont automatiquement d�truits.
  // Ex : p1
}
