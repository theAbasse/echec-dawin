/**
 * Mise en oeuvre de Piece.h
 *
 * @file Piece.cxx
 */

// A besoin de la declaration de la classe
#include <iostream>
#include "Piece.h"

// Pour utiliser les flux de iostream sans mettre "std::" tout le temps.
using namespace std;

Piece::Piece()
{
  cout << "Une Piece creee" << endl;
}


Piece::Piece(const Piece & autre)
{
  // autre.m_x=1;
  m_x=autre.m_x;
  m_y=autre.m_y;
  m_white=autre.m_white;
  cout << "Une Piece creee par copie" << endl;
}

Piece &
Piece::operator=(const Piece & autre)
{
  m_x=autre.m_x;
  m_y=autre.m_y;
  m_white=autre.m_white;
  cout << "Une Piece affectee" << endl;
  return *this;
}

Piece::~Piece()
{
  cout << "Une Piece detruite" << endl;
}

Piece::Piece( int x, int y, bool white )
{
  m_x = x;
  m_y = y;
  m_white = white;
  cout << "Une Piece creee specialisee" << endl;
}

void
Piece::init( int x, int y, bool white )
{
  m_x = x;
  m_y = y;
  m_white = white;
  cout << "Une Piece initialisee" << endl;
}

void
Piece::move( int x, int y )
{
  m_x = x;
  m_y = y;
}

int
Piece::x() const
{
  return m_x;
}

int
Piece::y() const
{
  return m_y;
}

bool
Piece::isWhite() const
{
  return m_white;
}

bool
Piece::isBlack() const
{
  return !m_white;
}

bool
Piece::mouvementValide(Echequier &e, int x, int y)
{
    cout << "mouvement valide Piece " << endl;
    return false;
}

void
Piece::affiche() const
{
  cout << "Piece: x=" << m_x << " y=" << m_y << " "
       << ( m_white ? "blanche" : "noire" ) << endl;
}
